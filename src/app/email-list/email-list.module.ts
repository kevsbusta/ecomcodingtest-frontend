import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailListComponent } from './email-list.component';
import { TableModule } from 'primeng/table';
import { EmailService } from './email-list.service';
import {DropdownModule} from 'primeng/dropdown';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    DropdownModule,
  ],
  declarations: [
    EmailListComponent
  ],
  exports: [
    EmailListComponent
  ],
  providers: [
    EmailService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EmailListModule { }
