export class EmailListResult {
  start: number;
  end: number;
  currentPage: number;
  totalCount: number;
  totalPage: number;
  results: EmailList[];
}

export class EmailList {
  id: string;
  emailLabel: string;
  emailLabelForDropdown: string;
  fromAddress: string;
  subject: string;
  templateText: string;
  emailType: EmailType;
  active: boolean;
  dateUpdated: Date;
  loadDrafts: boolean;
  parentId: string;
  versionCount: number;
  isDefault: boolean;
  bccAddress: string;
  isDraft: boolean;
  isNameValid: boolean;
  versions: EmailList[];
}

export enum EmailType {
  WelcomeEmail = 0,
  AccountActive = 1,
  ConfirmAccount = 2,
  ConfirmPasswordRequest = 3,
  NewPassword = 4,
  AdminApproval = 5,
  AdminUnlockRequest = 6,
  UnlockNotify = 7,
  GeneralEmail = 8
}
