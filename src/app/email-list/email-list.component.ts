import { Component, OnInit } from '@angular/core';
import { EmailList } from './email-list.model';
import { EmailService } from './email-list.service';

export interface EmailTypeItem {
  name: string;
  code: number;
}

@Component({
  selector: 'app-email-list',
  templateUrl: './email-list.component.html',
  styleUrls: ['./email-list.component.css']
})
export class EmailListComponent implements OnInit {

  emails_localdata: EmailList[];

  emails: EmailList[];

  cols: any[];

  valueTypes: EmailTypeItem[];

  selectedEmailType: EmailTypeItem;

  constructor(private emailService: EmailService) {
    this.cols = [
      { field: 'subject', header: 'Subject' },
      { field: 'templateText', header: 'Email Template' },
      { field: 'emailLabel', header: 'Email Label' },
      { field: 'fromAddress', header: 'From' },
      { field: 'dateUpdated', header: 'Date' }
    ];

    this.valueTypes = [
      {name: 'Welcome Email', code: 0},
      {name: 'Account Active', code: 1},
      {name: 'Confirm Account', code: 2},
      {name: 'Confirm Password Request', code: 3},
      {name: 'New Password', code: 4},
      {name: 'Admin Approval', code: 5},
      {name: 'Admin Unlock Request', code: 6},
      {name: 'Unlock Notify', code: 7},
      {name: 'General Email', code: 8},
    ];
  }

  ngOnInit() {
    this.emailService.getEmails().subscribe((res) => {
      this.emails_localdata = res.results;
      this.emails = res.results;
    });
  }

  onSort() {

  }

  private selectEmailType(emailType: EmailTypeItem) {
    if (emailType) {
      this.emails = this.emails_localdata.filter(email => email.emailType === emailType.code);
    } else {
      this.emails = this.emails_localdata;
    }
  }

}
