import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { EmailListResult } from './email-list.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class EmailService {

  constructor(private http: HttpClient) {}

  getEmails(): Observable<EmailListResult> {
    return this.http.get<EmailListResult>('http://localhost:5000/api/values');
  }
}
